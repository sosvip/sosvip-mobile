package vitor.eleoterio.com.frontend_mobile.model

data class Client(
    val id : Int,
    val name : String,
    val cpf : String,
    val telephone : Int,
    val birthdate : String,
    val email : String
)