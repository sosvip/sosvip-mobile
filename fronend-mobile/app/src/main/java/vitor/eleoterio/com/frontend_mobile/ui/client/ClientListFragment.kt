package vitor.eleoterio.com.frontend_mobile.ui.client

import SignupFragment
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import vitor.eleoterio.com.frontend_mobile.R

class ClientListFragment: Fragment() {
    private val nav by lazy { findNavController() }
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.order_list_fragment, container, false)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val clientAddClick = view.findViewById<ImageView>(R.id.icon_add)
        val clientEditClick = view.findViewById<ImageView>(R.id.icon_edit)
        val clientDeletClick = view.findViewById<ImageView>(R.id.icon_delet)

        clientAddClick.setOnClickListener{

            nav.navigate(R.id.clientListFragment)

        }

        clientEditClick.setOnClickListener{

            nav.navigate(R.id.clientListFragment)

        }

        clientDeletClick.setOnClickListener{

            nav.navigate(R.id.clientListFragment)

        }

    }
    private fun newOrderClickAdd(){

        val iconAddClick = Intent(requireActivity(), SignupFragment::class.java)
        startActivity(iconAddClick)
    }
    private fun editClientClick(){

        val iconEditClick = Intent(requireActivity(), SignupFragment::class.java)
        startActivity(iconEditClick)
    }
    private fun deletClientClick(){

        val iconDeletClick = Intent(requireActivity(), SignupFragment::class.java)
        startActivity(iconDeletClick)
    }
}