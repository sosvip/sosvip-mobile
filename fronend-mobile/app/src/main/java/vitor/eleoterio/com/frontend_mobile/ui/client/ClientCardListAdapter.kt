package vitor.eleoterio.com.frontend_mobile.ui.order

import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import vitor.eleoterio.com.frontend_mobile.R
import vitor.eleoterio.com.frontend_mobile.model.Client
import vitor.eleoterio.com.frontend_mobile.model.Order

class ClientCardListAdapter(
    private val orderList: List<Client>
) : RecyclerView.Adapter<ClientCardListAdapter.ClientCardViewHolder>() {

    class ClientCardViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(client: Client){
            itemView.findViewById<TextView>(R.id.text_client_name).text = client.name
            itemView.findViewById<TextView>(R.id.text_client_email).text = client.email

        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ClientCardViewHolder {
        val createdView = android.view.LayoutInflater.from(parent.context)
            .inflate(R.layout.order_card_model, parent, false)
        return ClientCardViewHolder(createdView)
    }

    override fun onBindViewHolder(holder: ClientCardViewHolder, position: Int) {
        val order = orderList.get(position)
        holder.bind(order)
    }

    override fun getItemCount(): Int {
        return orderList.size
    }
}

