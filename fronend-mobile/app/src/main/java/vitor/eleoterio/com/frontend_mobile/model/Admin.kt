package vitor.eleoterio.com.frontend_mobile.model

data class Admin(
    val username : String,
    val password : String
)