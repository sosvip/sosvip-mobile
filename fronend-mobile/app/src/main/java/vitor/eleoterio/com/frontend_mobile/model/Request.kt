package vitor.eleoterio.com.frontend_mobile.model

data class Request(
    val client_id : Int,
    var total_amount : Double,
    val description : String,
    var date_timestamp : Int,
    var status : Boolean
)

class RequestBuilder {
    var client_id : Int = 0
    var total_amount : Double = 0.0
    var description : String = ""
    var date_timestamp : Int = 0
    var status : Boolean = false

    fun build(): Request = Request(client_id, total_amount, description, date_timestamp, status)
}
