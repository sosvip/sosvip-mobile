package vitor.eleoterio.com.frontend_mobile.ui.order

import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import vitor.eleoterio.com.frontend_mobile.R
import vitor.eleoterio.com.frontend_mobile.model.Order

class OrderCardListAdapter(
    private val orderList: List<Order>
) : RecyclerView.Adapter<OrderCardListAdapter.OrderCardViewHolder>() {

    class OrderCardViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(order: Order){
            itemView.findViewById<TextView>(R.id.text_request).text = order.id.toString()
            itemView.findViewById<TextView>(R.id.text_request).text = order.user_id.toString()
            itemView.findViewById<TextView>(R.id.text_request).text = order.total_amount.toString()
            itemView.findViewById<TextView>(R.id.text_request).text = order.description
            itemView.findViewById<TextView>(R.id.text_request).text = order.date
            itemView.findViewById<TextView>(R.id.text_request).text = order.status.toString()
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OrderCardViewHolder {
        val createdView = android.view.LayoutInflater.from(parent.context)
            .inflate(R.layout.order_card_model, parent, false)
        return OrderCardViewHolder(createdView)
    }

    override fun onBindViewHolder(holder: OrderCardViewHolder, position: Int) {
        val order = orderList.get(position)
        holder.bind(order)
    }

    override fun getItemCount(): Int {
        return orderList.size
    }
}

