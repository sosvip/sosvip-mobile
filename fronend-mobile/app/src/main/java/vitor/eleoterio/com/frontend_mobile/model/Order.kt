package vitor.eleoterio.com.frontend_mobile.model

data class Order(
    val id : Int,
    val user_id : Int,
    val total_amount : Double,
    val description : String,
    val date : String,
    val status : Boolean
)
