package vitor.eleoterio.com.frontend_mobile.ui.login

import SignupFragment
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import vitor.eleoterio.com.frontend_mobile.R

class LoginFragment : Fragment() {

    private lateinit var viewModel: LoginViewModel
    private val nav by lazy { findNavController() }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.login_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val signupClick = view.findViewById<TextView>(R.id.textView_cadaster_click)

        signupClick.setOnClickListener{

            nav.navigate(R.id.clientListFragment)

        }

    }
    private fun signupClickEvent(){

        val signupScreen = Intent(requireActivity(), SignupFragment::class.java)
        startActivity(signupScreen)
    }
}